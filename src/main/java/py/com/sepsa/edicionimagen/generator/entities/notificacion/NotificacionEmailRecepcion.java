/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.notificacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "notificacion_email_recepcion", catalog = "sepsa", schema = "notificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotificacionEmailRecepcion.findAll", query = "SELECT n FROM NotificacionEmailRecepcion n"),
    @NamedQuery(name = "NotificacionEmailRecepcion.findById", query = "SELECT n FROM NotificacionEmailRecepcion n WHERE n.id = :id"),
    @NamedQuery(name = "NotificacionEmailRecepcion.findByGlnReceptor", query = "SELECT n FROM NotificacionEmailRecepcion n WHERE n.glnReceptor = :glnReceptor"),
    @NamedQuery(name = "NotificacionEmailRecepcion.findByTexto", query = "SELECT n FROM NotificacionEmailRecepcion n WHERE n.texto = :texto"),
    @NamedQuery(name = "NotificacionEmailRecepcion.findByEstado", query = "SELECT n FROM NotificacionEmailRecepcion n WHERE n.estado = :estado"),
    @NamedQuery(name = "NotificacionEmailRecepcion.findByObservacion", query = "SELECT n FROM NotificacionEmailRecepcion n WHERE n.observacion = :observacion")})
public class NotificacionEmailRecepcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "gln_receptor")
    private BigInteger glnReceptor;
    @Basic(optional = false)
    @Column(name = "asunto")
    private String asunto;
    @Basic(optional = false)
    @Column(name = "texto")
    private String texto;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "observacion")
    private String observacion;

    public NotificacionEmailRecepcion() {
    }

    public NotificacionEmailRecepcion(Integer id) {
        this.id = id;
    }

    public NotificacionEmailRecepcion(Integer id, BigInteger glnReceptor, String asunto, String texto, Character estado) {
        this.id = id;
        this.glnReceptor = glnReceptor;
        this.asunto = asunto;
        this.texto = texto;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getGlnReceptor() {
        return glnReceptor;
    }

    public void setGlnReceptor(BigInteger glnReceptor) {
        this.glnReceptor = glnReceptor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionEmailRecepcion)) {
            return false;
        }
        NotificacionEmailRecepcion other = (NotificacionEmailRecepcion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.notificacion.NotificacionEmailRecepcion[ id=" + id + " ]";
    }
    
}

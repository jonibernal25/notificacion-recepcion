/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_comprador", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoComprador.findAll", query = "SELECT p FROM ProductoComprador p"),
    @NamedQuery(name = "ProductoComprador.findByIdProducto", query = "SELECT p FROM ProductoComprador p WHERE p.productoCompradorPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoComprador.findByIdComprador", query = "SELECT p FROM ProductoComprador p WHERE p.productoCompradorPK.idComprador = :idComprador"),
    @NamedQuery(name = "ProductoComprador.findByEstado", query = "SELECT p FROM ProductoComprador p WHERE p.estado = :estado"),
    @NamedQuery(name = "ProductoComprador.findByFechaInsercion", query = "SELECT p FROM ProductoComprador p WHERE p.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "ProductoComprador.findByFechaModificacion", query = "SELECT p FROM ProductoComprador p WHERE p.fechaModificacion = :fechaModificacion")})
public class ProductoComprador implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoCompradorPK productoCompradorPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public ProductoComprador() {
    }

    public ProductoComprador(ProductoCompradorPK productoCompradorPK) {
        this.productoCompradorPK = productoCompradorPK;
    }

    public ProductoComprador(ProductoCompradorPK productoCompradorPK, Character estado) {
        this.productoCompradorPK = productoCompradorPK;
        this.estado = estado;
    }

    public ProductoComprador(int idProducto, int idComprador) {
        this.productoCompradorPK = new ProductoCompradorPK(idProducto, idComprador);
    }

    public ProductoCompradorPK getProductoCompradorPK() {
        return productoCompradorPK;
    }

    public void setProductoCompradorPK(ProductoCompradorPK productoCompradorPK) {
        this.productoCompradorPK = productoCompradorPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoCompradorPK != null ? productoCompradorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoComprador)) {
            return false;
        }
        ProductoComprador other = (ProductoComprador) object;
        if ((this.productoCompradorPK == null && other.productoCompradorPK != null) || (this.productoCompradorPK != null && !this.productoCompradorPK.equals(other.productoCompradorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.ProductoComprador[ productoCompradorPK=" + productoCompradorPK + " ]";
    }
    
}

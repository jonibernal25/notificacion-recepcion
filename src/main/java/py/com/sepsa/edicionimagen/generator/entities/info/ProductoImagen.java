/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_imagen", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoImagen.findAll", query = "SELECT p FROM ProductoImagen p"),
    @NamedQuery(name = "ProductoImagen.findById", query = "SELECT p FROM ProductoImagen p WHERE p.productoImagenPK.id = :id"),
    @NamedQuery(name = "ProductoImagen.findByIdProducto", query = "SELECT p FROM ProductoImagen p WHERE p.productoImagenPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoImagen.findByImgSrc", query = "SELECT p FROM ProductoImagen p WHERE p.imgSrc = :imgSrc"),
    @NamedQuery(name = "ProductoImagen.findByHashImagen", query = "SELECT p FROM ProductoImagen p WHERE p.hashImagen = :hashImagen"),
    @NamedQuery(name = "ProductoImagen.findByPrincipal", query = "SELECT p FROM ProductoImagen p WHERE p.principal = :principal"),
    @NamedQuery(name = "ProductoImagen.findByTipo", query = "SELECT p FROM ProductoImagen p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "ProductoImagen.findByNumero", query = "SELECT p FROM ProductoImagen p WHERE p.numero = :numero"),
    @NamedQuery(name = "ProductoImagen.findByTipoAlmacenamiento", query = "SELECT p FROM ProductoImagen p WHERE p.tipoAlmacenamiento = :tipoAlmacenamiento"),
    @NamedQuery(name = "ProductoImagen.findByAlto", query = "SELECT p FROM ProductoImagen p WHERE p.alto = :alto"),
    @NamedQuery(name = "ProductoImagen.findByAncho", query = "SELECT p FROM ProductoImagen p WHERE p.ancho = :ancho")})
public class ProductoImagen implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoImagenPK productoImagenPK;
    @Basic(optional = false)
    @Column(name = "img_src")
    private String imgSrc;
    @Basic(optional = false)
    @Column(name = "hash_imagen")
    private String hashImagen;
    @Column(name = "principal")
    private Character principal;
    @Basic(optional = false)
    @Column(name = "tipo")
    private Character tipo;
    @Column(name = "numero")
    private Integer numero;
    @Basic(optional = false)
    @Column(name = "tipo_almacenamiento")
    private int tipoAlmacenamiento;
    @Column(name = "alto")
    private Integer alto;
    @Column(name = "ancho")
    private Integer ancho;

    public ProductoImagen() {
    }

    public ProductoImagen(ProductoImagenPK productoImagenPK) {
        this.productoImagenPK = productoImagenPK;
    }

    public ProductoImagen(ProductoImagenPK productoImagenPK, String imgSrc, String hashImagen, Character tipo, int tipoAlmacenamiento) {
        this.productoImagenPK = productoImagenPK;
        this.imgSrc = imgSrc;
        this.hashImagen = hashImagen;
        this.tipo = tipo;
        this.tipoAlmacenamiento = tipoAlmacenamiento;
    }

    public ProductoImagen(int id, int idProducto) {
        this.productoImagenPK = new ProductoImagenPK(id, idProducto);
    }

    public ProductoImagenPK getProductoImagenPK() {
        return productoImagenPK;
    }

    public void setProductoImagenPK(ProductoImagenPK productoImagenPK) {
        this.productoImagenPK = productoImagenPK;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getHashImagen() {
        return hashImagen;
    }

    public void setHashImagen(String hashImagen) {
        this.hashImagen = hashImagen;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public int getTipoAlmacenamiento() {
        return tipoAlmacenamiento;
    }

    public void setTipoAlmacenamiento(int tipoAlmacenamiento) {
        this.tipoAlmacenamiento = tipoAlmacenamiento;
    }

    public Integer getAlto() {
        return alto;
    }

    public void setAlto(Integer alto) {
        this.alto = alto;
    }

    public Integer getAncho() {
        return ancho;
    }

    public void setAncho(Integer ancho) {
        this.ancho = ancho;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoImagenPK != null ? productoImagenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoImagen)) {
            return false;
        }
        ProductoImagen other = (ProductoImagen) object;
        if ((this.productoImagenPK == null && other.productoImagenPK != null) || (this.productoImagenPK != null && !this.productoImagenPK.equals(other.productoImagenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.ProductoImagen[ productoImagenPK=" + productoImagenPK + " ]";
    }
    
}

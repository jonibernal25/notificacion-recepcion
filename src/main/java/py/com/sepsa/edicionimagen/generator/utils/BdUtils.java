/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.utils;

import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.eclipse.persistence.platform.database.FirebirdPlatform;
import py.com.sepsa.edicionimagen.generator.entities.info.Contacto;
import py.com.sepsa.edicionimagen.generator.entities.info.ContactoEmail;
import py.com.sepsa.edicionimagen.generator.entities.info.Email;
import py.com.sepsa.edicionimagen.generator.entities.info.Local;
import py.com.sepsa.edicionimagen.generator.entities.notificacion.NotificacionEmailRecepcion;


/**
 *
 * @author Rubén Ortiz
 */
public class BdUtils {

    /**
     * Fábrica del manejador de entidades
     */
    private EntityManagerFactory emfSepsa = null;
    
    /**
     *
     *
     * @return Siguiente número de documento
     */
    public List<String> getDocDocNumRel(Integer docId) {
        EntityManager em = null;
        List<String> nextInvNum = null;
        try {
            em = getEntityManagerSepsa();
            javax.persistence.Query q = em.createNativeQuery("SELECT d2.nro_documento "
                    + "from trans.documento d1 join trans.cadena_documento cd on (d1.id = cd.id_doc and d1.id_tipo_documento = cd.id_tipo_doc) "
                    + "join trans.documento d2 on (d2.id = cd.id_doc_rel and d2.id_tipo_documento = cd.id_tipo_doc_rel) where d1.id = " + docId);
            nextInvNum = q.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return nextInvNum;
    }
    
    /**
     * Obtiene un local por GLN
     * @param gln GLN del local
     * @return Local
     */
    public Local findLocal(BigInteger gln) {

        if(gln == null) {
            return null;
        }
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Local> root = cq.from(Local.class);
        cq.select(root);

        cq.where(qb.equal(root.get("gln"), gln));

        javax.persistence.Query q = getEntityManagerSepsa()
                .createQuery(cq)
                .setHint("eclipselink.refresh", "true");

        List<Local> lista = q.getResultList();
        Local result = lista == null || lista.isEmpty()
                ? null
                : lista.get(0);
        return result;
    }
    
    /**
     * Obtiene la lista de notificación email recepción
     * @param glnReceptor GLN del receptor
     * @param estado Estado
     * @param first Primer resultado a retornar
     * @param max Cantidad máxima de resultados a retornar
     * @return Lista de notificación
     */
    public List<NotificacionEmailRecepcion> findNotif(BigInteger glnReceptor,
            Character estado, int first, int max) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<NotificacionEmailRecepcion> root = cq.from(NotificacionEmailRecepcion.class);
        cq.select(root);

        Predicate pred = qb.and();
        
        if(glnReceptor != null) {
            pred = qb.and(pred, qb.equal(root.get("glnReceptor"), glnReceptor));
        }
        
        if(estado != null) {
            pred = qb.and(pred, qb.equal(root.get("estado"), estado));
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManagerSepsa()
                .createQuery(cq)
                .setFirstResult(first)
                .setMaxResults(max)
                .setHint("eclipselink.refresh", "true");

        List<NotificacionEmailRecepcion> lista = q.getResultList();

        return lista;
    }
    
    /**
     * Obtiene la lista de contactos
     * @param idPersona Identificador de persona
     * @return Lista de contactos
     */
    public List<Contacto> findContacto(Integer idPersona) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Contacto> root = cq.from(Contacto.class);
        cq.select(root);

        Predicate pred = qb.and();
        
        if(idPersona != null) {
            pred = qb.and(pred, qb.equal(root.get("personaCollection").get("id"), idPersona));
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManagerSepsa()
                .createQuery(cq)
                .setHint("eclipselink.refresh", "true");

        List<Contacto> lista = q.getResultList();

        return lista;
    }
    
    /**
     * Obtiene la lista de contactos
     * @param idEmail Identificador de persona
     * @return Lista de contactos
     */
    public Email findEmail(Integer idEmail) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Email> root = cq.from(Email.class);
        cq.select(root);

        Predicate pred = qb.and();
        
        if(idEmail != null) {
            pred = qb.and(pred, qb.equal(root.get("id"), idEmail));
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManagerSepsa()
                .createQuery(cq)
                .setHint("eclipselink.refresh", "true");

        List<Email> lista = q.getResultList();

        Email result = lista == null || lista.isEmpty()
                ? null
                : lista.get(0);
        
        return result;
    }
    
    /**
     * Obtiene la lista de contacto email
     * @param idContacto Identificador de contacto
     * @return Lista de contacto email
     */
    public List<ContactoEmail> findContactoEmail(Integer idContacto) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<ContactoEmail> root = cq.from(ContactoEmail.class);
        cq.select(root);

        Predicate pred = qb.and();
        
        if(idContacto != null) {
            pred = qb.and(pred, qb.equal(root.get("contactoEmailPK").get("idContacto"), idContacto));
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManagerSepsa()
                .createQuery(cq)
                .setHint("eclipselink.refresh", "true");

        List<ContactoEmail> lista = q.getResultList();

        return lista;
    }

    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public <T> boolean edit(T documento) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.merge(documento);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public <T> boolean create(T documento) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.persist(documento);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public Object find(Class clazz, Object id) {
        Object t = null;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            t = em.find(clazz, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return t;
    }
    
    private EntityManager getEntityManagerSepsa() {
        if(emfSepsa == null) {
            emfSepsa = JpaUtil.getEntityManagerFactorySepsa();
        }
        return emfSepsa.createEntityManager();
    }
    
    /*
    * Constructor
     */
    public BdUtils() {}

}

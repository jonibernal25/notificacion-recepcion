/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findById", query = "SELECT p FROM Producto p WHERE p.id = :id"),
    @NamedQuery(name = "Producto.findByDescripcion", query = "SELECT p FROM Producto p WHERE p.descripcion = :descripcion"),
    @NamedQuery(name = "Producto.findByCodigoGtin", query = "SELECT p FROM Producto p WHERE p.codigoGtin = :codigoGtin"),
    @NamedQuery(name = "Producto.findByCodigoInterno", query = "SELECT p FROM Producto p WHERE p.codigoInterno = :codigoInterno"),
    @NamedQuery(name = "Producto.findByIdCategoria", query = "SELECT p FROM Producto p WHERE p.idCategoria = :idCategoria"),
    @NamedQuery(name = "Producto.findByIdTipoProducto", query = "SELECT p FROM Producto p WHERE p.idTipoProducto = :idTipoProducto"),
    @NamedQuery(name = "Producto.findByDescripcionLarga", query = "SELECT p FROM Producto p WHERE p.descripcionLarga = :descripcionLarga"),
    @NamedQuery(name = "Producto.findByEstado", query = "SELECT p FROM Producto p WHERE p.estado = :estado"),
    @NamedQuery(name = "Producto.findByPorcentajeImpuesto", query = "SELECT p FROM Producto p WHERE p.porcentajeImpuesto = :porcentajeImpuesto"),
    @NamedQuery(name = "Producto.findByUnidadMinimaVenta", query = "SELECT p FROM Producto p WHERE p.unidadMinimaVenta = :unidadMinimaVenta"),
    @NamedQuery(name = "Producto.findByUnidadMedida", query = "SELECT p FROM Producto p WHERE p.unidadMedida = :unidadMedida"),
    @NamedQuery(name = "Producto.findByFechaModificacionImagen", query = "SELECT p FROM Producto p WHERE p.fechaModificacionImagen = :fechaModificacionImagen"),
    @NamedQuery(name = "Producto.findByDescripcionProveedor", query = "SELECT p FROM Producto p WHERE p.descripcionProveedor = :descripcionProveedor"),
    @NamedQuery(name = "Producto.findByDescripcionCorta", query = "SELECT p FROM Producto p WHERE p.descripcionCorta = :descripcionCorta")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "codigo_gtin")
    private String codigoGtin;
    @Column(name = "codigo_interno")
    private String codigoInterno;
    @Column(name = "id_categoria")
    private Integer idCategoria;
    @Column(name = "id_tipo_producto")
    private Integer idTipoProducto;
    @Column(name = "descripcion_larga")
    private String descripcionLarga;
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "porcentaje_impuesto")
    private BigInteger porcentajeImpuesto;
    @Basic(optional = false)
    @Column(name = "unidad_minima_venta")
    private BigInteger unidadMinimaVenta;
    @Basic(optional = false)
    @Column(name = "unidad_medida")
    private String unidadMedida;
    @Column(name = "fecha_modificacion_imagen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacionImagen;
    @Column(name = "descripcion_proveedor")
    private String descripcionProveedor;
    @Column(name = "descripcion_corta")
    private String descripcionCorta;
    @JoinTable(name = "bandeo", schema = "info", joinColumns = {
        @JoinColumn(name = "id_producto_bandeo", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_producto", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Producto> productoCollection;
    @ManyToMany(mappedBy = "productoCollection")
    private Collection<Producto> productoCollection1;
    @JoinColumn(name = "id_marca", referencedColumnName = "id")
    @ManyToOne
    private Marca idMarca;
    @JoinColumn(name = "id_persona", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Persona idPersona;

    public Producto() {
    }

    public Producto(Integer id) {
        this.id = id;
    }

    public Producto(Integer id, String descripcion, String codigoGtin, BigInteger porcentajeImpuesto, BigInteger unidadMinimaVenta, String unidadMedida) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigoGtin = codigoGtin;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.unidadMinimaVenta = unidadMinimaVenta;
        this.unidadMedida = unidadMedida;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getDescripcionLarga() {
        return descripcionLarga;
    }

    public void setDescripcionLarga(String descripcionLarga) {
        this.descripcionLarga = descripcionLarga;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public BigInteger getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(BigInteger porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public BigInteger getUnidadMinimaVenta() {
        return unidadMinimaVenta;
    }

    public void setUnidadMinimaVenta(BigInteger unidadMinimaVenta) {
        this.unidadMinimaVenta = unidadMinimaVenta;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Date getFechaModificacionImagen() {
        return fechaModificacionImagen;
    }

    public void setFechaModificacionImagen(Date fechaModificacionImagen) {
        this.fechaModificacionImagen = fechaModificacionImagen;
    }

    public String getDescripcionProveedor() {
        return descripcionProveedor;
    }

    public void setDescripcionProveedor(String descripcionProveedor) {
        this.descripcionProveedor = descripcionProveedor;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public void setDescripcionCorta(String descripcionCorta) {
        this.descripcionCorta = descripcionCorta;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection1() {
        return productoCollection1;
    }

    public void setProductoCollection1(Collection<Producto> productoCollection1) {
        this.productoCollection1 = productoCollection1;
    }

    public Marca getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Marca idMarca) {
        this.idMarca = idMarca;
    }

    public Persona getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Persona idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.edicionimagen.generator.entities.info.Producto[ id=" + id + " ]";
    }
    
}

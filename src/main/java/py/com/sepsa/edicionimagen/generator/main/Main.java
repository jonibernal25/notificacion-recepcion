/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.edicionimagen.generator.main;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import py.com.sepsa.edicionimagen.generator.entities.info.Contacto;
import py.com.sepsa.edicionimagen.generator.entities.info.ContactoEmail;
import py.com.sepsa.edicionimagen.generator.entities.info.Email;
import py.com.sepsa.edicionimagen.generator.entities.info.Local;
import py.com.sepsa.edicionimagen.generator.entities.notificacion.NotificacionEmailRecepcion;
import py.com.sepsa.edicionimagen.generator.utils.BdUtils;
import py.com.sepsa.edicionimagen.generator.utils.FileUtils;
import py.com.sepsa.edicionimagen.generator.utils.JpaUtil;
import py.com.sepsa.edicionimagen.generator.utils.LogHandler;
import py.com.sepsa.edicionimagen.generator.utils.MailUtils;
import py.com.sepsa.edicionimagen.generator.utils.Report;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Main {

    /**
     * Ruta al archivo de configuración del sistema
     */
    private static String config = "config.json";
    //private static String config = "C:\\Users\\Sepsa\\Desktop\\EdicionImagen\\config.json";
    
    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

        File fileConfig = new File(config);

        if (!fileConfig.exists()) {
            System.out.println("No existe el archivo de configuración");
            System.exit(1);
        }

        String configData = IOUtils.toString(new FileInputStream(fileConfig));

        JSONObject json = new JSONObject(configData);
        JpaUtil.setURL_SEPSA(json.getString("urlSepsa"));
        JpaUtil.setDB_USER_SEPSA(json.getString("userSepsa"));
        JpaUtil.setDB_PASS_SEPSA(json.getString("passwordSepsa"));
        Long sleepTime = json.optLong("sleepTime", 30000);

        while (true) {

            BdUtils bdUtils = new BdUtils();

            try {

                List<NotificacionEmailRecepcion> list = bdUtils
                        .findNotif(null, 'P', 0, 10);

                while (list != null && !list.isEmpty()) {
                    
                    for (NotificacionEmailRecepcion elem : list) {

                        LogHandler.logDebug(String.format("Generando Notificación para Id: %d, GLN:%d",
                                elem.getId(), elem.getGlnReceptor()));

                        Local local = bdUtils.findLocal(elem.getGlnReceptor());

                        if(local == null) {
                            LogHandler.logError(String.format("No existe local para el GLN:%d",
                                    elem.getGlnReceptor()));
                            elem.setEstado('E');
                            elem.setObservacion("No existe local");
                            bdUtils.edit(elem);
                            continue;
                        }
                        
                        List<Contacto> contactos = bdUtils
                                .findContacto(local.getLocalPK()
                                        .getIdPersona());
                        
                        if(contactos == null || contactos.isEmpty()) {
                            LogHandler.logError(String.format("No existen contactos para el IdPersona:%d",
                                    local.getLocalPK().getIdPersona()));
                            elem.setEstado('E');
                            elem.setObservacion("No existen contactos");
                            bdUtils.edit(elem);
                            continue;
                        }
                        
                        List<String> mails = new ArrayList<>();
                        
                        for (Contacto contacto : contactos) {
                            
                            List<ContactoEmail> contactoEmails = bdUtils
                                    .findContactoEmail(contacto.getIdContacto());
                            
                            for (ContactoEmail contactoEmail : contactoEmails) {
                                
                                if(contactoEmail.getNotificacionDocumento().equals('S')) {
                                    
                                    Email email = bdUtils.findEmail(contactoEmail
                                            .getContactoEmailPK().getIdEmail());
                                    
                                    if(email != null) {
                                        mails.add(email.getEmail());
                                    }
                                }
                                
                            }
                            
                        }
                        
                        if(mails.isEmpty()) {
                            LogHandler.logError(String.format("No existen contactos para documentos para el IdPersona:%d",
                                local.getLocalPK().getIdPersona()));
                            elem.setEstado('E');
                            elem.setObservacion("No existen contactos para documentos");
                            bdUtils.edit(elem);
                            continue;
                        }

                        try {
                            
                            sendMail(mails, elem.getAsunto(), elem.getTexto());
                            elem.setEstado('C');
                            bdUtils.edit(elem);
                            LogHandler.logDebug(String.format("Notificación generada para el Id: %d, GLN:%d",
                                    elem.getId(), elem.getGlnReceptor()));
                            
                        } catch(Exception ex) {
                            
                            elem.setEstado('E');
                            bdUtils.edit(elem);
                            LogHandler.logFatal(String.format("Error al generar notificación para el Id: %d, GLN:%d",
                                    elem.getId(), elem.getGlnReceptor()), ex);
                            
                        }

                    }

                    list.clear();
                    list = bdUtils.findNotif(null, 'P', 0, 10);
                }
            } catch(Exception ex) {
                LogHandler.logFatal("Se ha producido un error:", ex);
            }

            try {
                LogHandler.logInfo(String.format("Durmiendo el hilo de"
                        + " transferencia por %d milisegundos", sleepTime));
                Thread.sleep(sleepTime);
            } catch (Exception e) {}
        }
    }
    
    /**
     * Realiza el envío de mails
     * @param mails Lista de emails
     * @param asunto Asunto del mensaje
     * @param text Texto del correo
     */
    private static void sendMail(List<String> mails, String asunto, String text) throws Exception {
        Report t = new Report();
        t.setName("Comercial");
        t.setMailToList(mails);
        List<Report> tos = new ArrayList();
        tos.add(t);
        MailUtils.send(tos, text, asunto);
    }
    
}
